/*
Clase de C++ escrita con la finalidad de calcular la velocidad
del viento medida por un anemómetro impreso en 3D. Las RPM son
contadas por un tacometro basado en el chip LM393.
La clase tiene como finalidad calcular la velocidad angular en rad/s
y la velocidad tangencial en m/s y Km/h

Autor: Daniel Omar Vergara Pérez
Director General de Andros
*/

#ifndef ANEMOMETER_H
#define ANEMOMETER_H

#if ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
  #include "pins_arduino.h"
  #include "WConstants.h"
#endif

// Your class header here...

class Anemometer
{
    public:
        //Constructor
        Anemometer();
        //Obitnene la constante Pi
        float getPi();
        //Determina el valor del contador por cada vuelta del anemómetro
        void setCounter();
        //Regresa el valor del contador
        unsigned int getCounter();
        //Retorna el radio del anemometro
        float getRadioA();
        //Calcula la velocidad Angular en radianes sobre segundo
        void setVelAngular();
        //Obtiene el valor de la velocidad angular
        float getVelAngular();

        //calcula la velocidad del viento, basandose en la fórmula
        //de la velocidad Tangencial, la unidad es m/s
        void setVelTangencialMS();

        float getVelTangencialMS();
        //Calcula la velocidad del viento en KM/h
        void setVelTangencialKMH();

        float getVelTangencialKMH();

        void retToZeroCounter();

        void printVelAngular();
        void printVelTangMS();
        void printVelTangKMH();

    protected:

    private:
        float velAngular;
        float velTangencialMS;
        float velTangencialKMH;
        unsigned int counter;
        float m_pi;
        float radioA; //radio en m
};

#endif // ANEMOMETER_H
