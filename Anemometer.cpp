#include "Anemometer.h"

//Constructor de la clase Anemometer
Anemometer::Anemometer()
{
    //ctor
    m_pi = 3.1416; //Constante matemática Pi
    radioA = 0.84;  //radio del anemómetro en m
    //contador, almacena el número de veces que las
    // patitas del la base del anemómetro pasan por el tacómetro
    counter = 0;
    velAngular = 0.0;
    velTangencialMS = 0.0;
    velTangencialKMH = 0.0;
}

//Retorna el valor de la constante PI
float Anemometer::getPi(){
    return m_pi;
}
//incrementa el valor del contador cada que las patitas
//del anemómetro pasan por el tacómetro. Tiene 2.
void Anemometer::setCounter(){
    counter++;
}
//Retorna el valor del contador
unsigned int Anemometer::getCounter(){
    return counter;
}
//Retorna el valor del radio del anemómetro en metros
float Anemometer::getRadioA(){
    return radioA;
}
//Calcular la velocidad angular en rad/s.
//Fórmula vAng = 2 * pi * f donde f es n/t o counter/2/segundo
//No es necesario dividir entre el tiempo, ya que esta función se llama cada segundo.
void Anemometer::setVelAngular(){
    this-> velAngular = 0;
    this-> velAngular = 2 * this-> getPi() * this-> getCounter()/8;
    //Serial.print("Velocidad angular (rad/s): ");
    //Serial.print(this-> getVelAngular());
    //Serial.println(" rad/s");
}

float Anemometer::getVelAngular(){

    return this-> velAngular;
}

//Establece el valor de counter a cero
void Anemometer::retToZeroCounter(){
    this-> counter = 0;
}

//Calcula el valor de la velocidad tangencial en m/s
//Fórmula vT = vAng * r
//Aquí se establece el valor de counter en cero, para volver a contar
//las revoluciones por segundo, en el próximo intervalo de tiempo(1 segundo)
void Anemometer::setVelTangencialMS(){
    this-> velTangencialMS = 0;
    //this-> setVelAngular();
    this->velTangencialMS = this-> getVelAngular() * this-> getRadioA();

        //return velTangencialMS;
}

float Anemometer::getVelTangencialMS(){
    return this-> velTangencialMS;
}

//Convierte la velocidad en m/s a Km/h
void Anemometer::setVelTangencialKMH(){
    this-> velTangencialKMH = 0;
    //this-> setVelTangencialMS();
    float tempo= this-> getVelTangencialMS();
    this-> velTangencialKMH  = tempo * 1/1000 * 3600;
    this-> retToZeroCounter();
}

float Anemometer::getVelTangencialKMH(){

    return this-> velTangencialKMH;
}


void Anemometer::printVelAngular(){
    //Serial.print("Velocidad angular (rad/s): ");
    Serial.print(this-> getVelAngular());
    Serial.println("R");

}

void Anemometer::printVelTangMS(){
    //Serial.print("Velocidad tangencial (m/s): ");
    Serial.print(this->getVelTangencialMS());
    Serial.println("E"); //km/s
}

void Anemometer::printVelTangKMH(){
    //Serial.print("Velocidad tangencial (Km/h): ");
    Serial.print(this->getVelTangencialKMH());
    Serial.println("K"); //km/s
}
