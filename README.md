Anemometer
===

*Librería de C++ para Arduino creada para usarse con un optoacploador basado en el LM393 y un anemometro impreso en 3D cuya finalidad es cualcular la velocidad del viento*

![Alt text] (Anemometer2_0.jpeg)
Anemómetro con circuito a la derecha.

La librería es una clase C++, que está compuesta por dos archivos .h y .cpp.

Anemometer.h
---
En la cabecera se inicia de la siguiente forma:

```cpp
#ifndef ANEMOMETER_H
#define ANEMOMETER_H
#if ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
  #include "pins_arduino.h"
  #include "WConstants.h"
#endif
```
Se puede aprecia que se utilizan la librería de Arduino para poder usar métodos de su lenguaje en C++.

Los métodos de clase son públicos:

```cpp
 public:
        //Constructor
        Anemometer();
        //Obitnene la constante Pi
        float getPi();
        //Determina el valor del contador por cada vuelta del anemómetro
        void setCounter();
        //Regresa el valor del contador
        unsigned int getCounter();
        //Retorna el radio del anemometro
        float getRadioA();
        //Calcula la velocidad Angular en radianes sobre segundo
        float setVelAngular();
        //calcula la velocidad del viento, basandose en la fórmula
        //de la velocidad Tangencial, la unidad es m/s
        float setVelTangencialMS();
        //Calcula la velocidad del viento en KM/h
        float setVelTangencialKMH();
        void retToZeroCounter();

```
Las variables de clase son privadas:

```cpp
    private:
        float velAngular;
        float velTangencialMS;
        float velTangencialKMH;
        unsigned int counter;
        float m_pi;
        float radioA; //radio en m
```

Anemometer.cpp
---
En el .cpp es donde se coloca la lógica de la clase. Lo primero a considerar es el constructor, donde se inicializan las variables:

```cpp
//Constructor de la clase Anemometer
Anemometer::Anemometer()
{
    //ctor
    m_pi = 3.1416; //Constante matemática Pi
    radioA = 0.084;  //radio del anemómetro en m
    //contador, almacena el número de veces que las
    // patitas del la base del anemómetro pasan por el tacómetro
    counter = 0;
    velAngular = 0.0;
    velTangencialMS = 0.0;
    velTangencialKMH = 0.0;
}
```
Lo siguiente, es la función que se utiliza para el valor de pi ya que es privada:

```cpp
//Retorna el valor de la constante PI
float Anemometer::getPi(){
    return m_pi;
}
```
A continuación, se establece y obtiene el contador. Esto indica las veces que el optoacoplador registra una interrupción en el sensor.
:

```cpp
//incrementa el valor del contador cada que las patitas
//del anemómetro pasan por el tacómetro. Tiene 2.
void Anemometer::setCounter(){
    counter++;
}
```

```cpp
//Retorna el valor del contador
unsigned int Anemometer::getCounter(){
    return counter;
}
```
El radio del anemometro está estandarizado y es inicializado en el constructor. Para obtener ese valor privado se utiliza la siguiene función:

```cpp
//Retorna el valor del radio del anemómetro en metros
float Anemometer::getRadioA(){
    return radioA;
}
```
Es importante determinar el valor de la velocidad angular del anemómetro para calcular la velocidad Tangencial, posteriormente.
```cpp
//Calcular la velocidad angular en rad/s.
//Fórmula vAng = 2 * pi * f donde f es n/t o counter/8/segundo
//No es necesario dividir entre el tiempo, ya que esta función se llama cada segundo.
float Anemometer::setVelAngular(){
    velAngular = 0;
    velAngular = 2 * getPi() * getCounter()/8;
    Serial.print("Velocidad angular (rad/s): ");
    Serial.print(velAngular);
    Serial.println(" rad/s");
    return velAngular;
}
```
Es necesario regresar el contador a cero, cada determinado tiempo (en esta caso cada segundo)

```cpp
//Establece el valor de counter a cero
void Anemometer::retToZeroCounter(){
    counter = 0;
}
```
**Nota**: Por la simplicidad del esta librería no se usan gets en la siguientes operaciones. Pero si se implementarán en commits posteriores. 

Para calcular la velocidad tangencial en m/s, es necesario utilizar este método:

```cpp
//Calcula el valor de la velocidad tangencial en m/s
//Fórmula vT = vAng * r
//Aquí se establece el valor de counter en cero, para volver a contar
//las revoluciones por segundo, en el próximo intervalo de tiempo(1 segundo)
float Anemometer::setVelTangencialMS(){
    velTangencialMS = 0;
    velTangencialMS = setVelAngular() * getRadioA();
    retToZeroCounter();
    Serial.print("Velocidad tangencial (m/s): ");
    Serial.print(velTangencialMS);
    Serial.println(" m/s");
    return velTangencialMS;
}
```
Y en Km/h:

```cpp
//Convierte la velocidad en m/s a Km/h
float Anemometer::setVelTangencialKMH(){
    velTangencialKMH = 0;
    velTangencialKMH  = velTangencialMS * (1/1000) * (3600);
    Serial.print("Velocidad tangencial (Km/h): ");
    Serial.print(velTangencialKMH);
    Serial.println(" Km/h");
    return velTangencialKMH;
}
```